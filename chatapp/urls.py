from django.urls import path

from chatapp.views import register, my_profile, profiles, profile_detail, create_chat_request, get_chat_requests, \
    chat_friends, accept_chat_request, unfriend, create_chat

urlpatterns = [
    path('register/', register, name='register'),
    path('my_profile/', my_profile, name='my_profile'),
    path('profiles/', profiles, name='profiles'),
    path('profiles/<int:pk>', profile_detail, name='profile_detail'),
    path('create_chat_request/<int:profile_id>', create_chat_request, name='create_chat_request'),
    path('chat_requests', get_chat_requests, name='chat_requests'),
    path('chat_friends', chat_friends, name='chat_friends'),
    path('accept_chat_request/<int:pk>', accept_chat_request, name='accept_chat_request'),
    path('unfriend/<int:pk>', unfriend, name='unfriend'),
    path('create_chat/<int:pk>', create_chat, name='create_chat')
]