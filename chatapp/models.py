from PIL import Image
from django.contrib.auth.models import User
from django.db import models


# Create your models here.
from django_resized import ResizedImageField


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    fullname = models.CharField(max_length=255, null=True)
    age = models.IntegerField(null=True)
    city = models.CharField(max_length=255, null=True)
    country = models.CharField(max_length=255, null=True)
    image = models.ImageField(default='default.png', upload_to='profile_pics')

    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        super(Profile, self).save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 200 or img.width > 200:
            output_size = (200, 200)
            img.thumbnail(output_size)
            img.save(self.image.path)


class ChatRequest(models.Model):
    target = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='target_profile')
    source = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='source_profile')
    is_accepted = models.BooleanField(default=False)

    def __str__(self):
        return f'Source: {self.source} Target: {self.target}'


class Chat(models.Model):
    sender = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='chat_sender', null=False)
    receiver = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='chat_receiver', null=False)
    message = models.TextField(null=False)
    created_at = models.DateTimeField(auto_now_add=True, null=False)

    def __str__(self):
        return f'Sender: {self.sender} Receiver: {self.receiver}'

