from itertools import chain

from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

# Create your views here.
from chatapp.forms import RegisterForm, UserUpdateForm, ProfileUpdateForm, CreateChatForm
from chatapp.models import Profile, ChatRequest, Chat
from chatapp.serializers import ChatSerializer


def home(request):
    return render(request, 'home/home.html')


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            messages.info(request, "Thanks for registering. You are now logged in.")
            new_user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'],
                                    )
            login(request, new_user)
            return HttpResponseRedirect("/chatapp/my_profile")
    else:
        form = RegisterForm()

    return render(request, 'register/register.html', {'form': form})


@login_required
def my_profile(request):
    if request.method == 'POST':
        user_form = UserUpdateForm(request.POST, instance=request.user)
        profile_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('/chatapp/my_profile/')
    else:
        user_form = UserUpdateForm(instance=request.user)
        profile_form = ProfileUpdateForm(instance=request.user.profile)

    context = {
        'user_form' : user_form,
        'profile_form' : profile_form
    }

    return render(request, 'home/myprofile.html', context)


@login_required()
def profiles(request):
    user_profiles = Profile.objects.filter(~Q(id=request.user.profile.id))

    context = {
        'profiles' : user_profiles
    }

    return render(request, 'home/profiles.html', context)


@login_required()
def profile_detail(request, pk):
    current_user = request.user
    profile = Profile.objects.get(pk=pk)
    from_current_user_chat_requests = ChatRequest.objects.filter(source=request.user.profile, target=profile)
    to_current_user_chat_requests = ChatRequest.objects.filter(source=profile, target=request.user.profile)

    all_current_user_chat_requests = from_current_user_chat_requests | to_current_user_chat_requests

    sent_chats = Chat.objects.filter(sender=request.user.profile, receiver=profile)
    received_chats = Chat.objects.filter(sender=profile, receiver=request.user.profile)

    all_current_user_chats = sent_chats | received_chats

    if len(from_current_user_chat_requests) == 0 and len(to_current_user_chat_requests) == 0:
        request_exists = False
    else:
        request_exists = True

    context = {
        'profile': profile,
        'request_exists': request_exists,
        'from_current_user_chat_requests': from_current_user_chat_requests,
        'to_current_user_chat_requests': to_current_user_chat_requests,
        'all_current_user_chat_requests': all_current_user_chat_requests,
        'all_current_user_chats': all_current_user_chats,
        'current_user': current_user
    }

    return render(request, 'home/profile_detail.html', context)


@login_required()
def create_chat_request(request, profile_id):
    profile = Profile.objects.get(pk=profile_id)
    request_exists = ChatRequest.objects.filter(source=request.user.profile, target=profile)

    if len(request_exists) == 0:
        chat_request = ChatRequest(source=request.user.profile, target=profile)
        chat_request.save()

    return redirect(f'/chatapp/profiles/{ profile_id }')


@login_required()
def get_chat_requests(request):
    chat_requests = ChatRequest.objects.filter(target=request.user.profile, is_accepted=False)

    context = {
        'chat_requests' : chat_requests
    }

    return render(request, 'home/chat_requests.html', context)


@login_required()
def chat_friends(request):
    requests_from_current_user = ChatRequest.objects.filter(is_accepted=True, source=request.user.profile)
    requests_to_current_user = ChatRequest.objects.filter(is_accepted=True, target=request.user.profile)

    context = {
        'requests_from_current_user': requests_from_current_user,
        'requests_to_current_user': requests_to_current_user
    }

    return render(request, 'home/chat_friends.html', context)


@login_required()
def accept_chat_request(request, pk):
    ChatRequest.objects.filter(pk=pk).update(is_accepted=True)
    return redirect('/chatapp/chat_requests')


@login_required()
def unfriend(request, pk):
    ChatRequest.objects.filter(pk=pk).update(is_accepted=False)
    return redirect("/chatapp/profiles/")


@login_required()
def create_chat(request, pk):
    if request.method == 'POST':
        profile = Profile.objects.get(pk=pk)
        message= request.POST.get('message')
        chat = Chat(sender=request.user.profile, receiver=profile, message=message)
        chat.save()

    return redirect(f'/chatapp/profiles/{pk}')