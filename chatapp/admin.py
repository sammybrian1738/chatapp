from django.contrib import admin

# Register your models here.
from chatapp.models import Profile, ChatRequest, Chat

admin.site.register(Profile)
admin.site.register(ChatRequest)
admin.site.register(Chat)